#ifndef CERT_H_
#define CERT_H_

//download from
//GlobalSign RootCA valid until 2028/01/28
static const char rootCA[] =
    "-----BEGIN CERTIFICATE-----\r\n"
    "MIIDdTCCAl2gAwIBAgILBAAAAAABFUtaw5QwDQYJKoZIhvcNAQEFBQAwVzELMAkG\r\n"
    "A1UEBhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24gbnYtc2ExEDAOBgNVBAsTB1Jv\r\n"
    "b3QgQ0ExGzAZBgNVBAMTEkdsb2JhbFNpZ24gUm9vdCBDQTAeFw05ODA5MDExMjAw\r\n"
    "MDBaFw0yODAxMjgxMjAwMDBaMFcxCzAJBgNVBAYTAkJFMRkwFwYDVQQKExBHbG9i\r\n"
    "YWxTaWduIG52LXNhMRAwDgYDVQQLEwdSb290IENBMRswGQYDVQQDExJHbG9iYWxT\r\n"
    "aWduIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDaDuaZ\r\n"
    "jc6j40+Kfvvxi4Mla+pIH/EqsLmVEQS98GPR4mdmzxzdzxtIK+6NiY6arymAZavp\r\n"
    "xy0Sy6scTHAHoT0KMM0VjU/43dSMUBUc71DuxC73/OlS8pF94G3VNTCOXkNz8kHp\r\n"
    "1Wrjsok6Vjk4bwY8iGlbKk3Fp1S4bInMm/k8yuX9ifUSPJJ4ltbcdG6TRGHRjcdG\r\n"
    "snUOhugZitVtbNV4FpWi6cgKOOvyJBNPc1STE4U6G7weNLWLBYy5d4ux2x8gkasJ\r\n"
    "U26Qzns3dLlwR5EiUWMWea6xrkEmCMgZK9FGqkjWZCrXgzT/LCrBbBlDSgeF59N8\r\n"
    "9iFo7+ryUp9/k5DPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8E\r\n"
    "BTADAQH/MB0GA1UdDgQWBBRge2YaRQ2XyolQL30EzTSo//z9SzANBgkqhkiG9w0B\r\n"
    "AQUFAAOCAQEA1nPnfE920I2/7LqivjTFKDK1fPxsnCwrvQmeU79rXqoRSLblCKOz\r\n"
    "yj1hTdNGCbM+w6DjY1Ub8rrvrTnhQ7k4o+YviiY776BQVvnGCv04zcQLcFGUl5gE\r\n"
    "38NflNUVyRRBnMRddWQVDf9VMOyGj/8N7yy5Y0b2qvzfvGn9LhJIZJrglfCm7ymP\r\n"
    "AbEVtQwdpf5pLGkkeB6zpxxxYu7KyJesF12KwvhHhm4qxFYxldBniYUr+WymXUad\r\n"
    "DKqC5JlR3XC321Y9YeRq4VzW9v493kHMB65jUr9TU/Qr6cf9tveCX4XSQRjbgbME\r\n"
    "HMUfpIBvFSDJ3gyICh3WZlXi/EjJKSZp4A==\r\n"
    "-----END CERTIFICATE-----\r\n";

//created by openssl
//openssl req -newkey rsa:2048 -sha256 -nodes -subj -keyout client.key -out client.csr
//openssl x509 -req -sha256 -days 1095 -in client.csr -signkey client.key -out client.crt
//  client.crt => certificate[]
//  client.key => privateKey[]
static const char certificate[] =
    "-----BEGIN CERTIFICATE-----\r\n"
    "MIIDYDCCAkgCCQDt5auVzuuN7zANBgkqhkiG9w0BAQsFADByMQswCQYDVQQGEwJK\r\n"
    "UDEPMA0GA1UECAwGTmFnYW5vMRAwDgYDVQQHDAdBenVtaW5vMRYwFAYDVQQKDA1D\r\n"
    "aXJjdWl0RGVzaWduMQ8wDQYDVQQLDAZDbGllbnQxFzAVBgNVBAMMDmRvY29kZW1v\r\n"
    "LmxvY2FsMB4XDTIxMDkyNzA3MTYzNFoXDTMxMDkyNTA3MTYzNFowcjELMAkGA1UE\r\n"
    "BhMCSlAxDzANBgNVBAgMBk5hZ2FubzEQMA4GA1UEBwwHQXp1bWlubzEWMBQGA1UE\r\n"
    "CgwNQ2lyY3VpdERlc2lnbjEPMA0GA1UECwwGQ2xpZW50MRcwFQYDVQQDDA5kb2Nv\r\n"
    "ZGVtby5sb2NhbDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOhzJHTt\r\n"
    "avc/u0800YR6ir7/OGJEyPXbAf3NM2Cm8wER9vlt6IKrJ1IJBNhLY/vILAOcHcdk\r\n"
    "nOcNpvBU/GCK0ysIPsqQ5tYV642k0wb/uZL2UAj3Okt5i/zCyOu2VRoy0luugsI0\r\n"
    "Lxp7U0UqQQpop0uvYBpLjEWV3q8ntKhuJAL32qMVohNvIFZ7sMuvUYDvvnTiuK6r\r\n"
    "+NkiIvqsMH6kJqwggGvgWU40jasZaFLGkjncWyCwIfPt1JkJR+Kb06C6JW65jh90\r\n"
    "W0cqPA5Ca6hgDYMIsDw/ufJLL+8wTPlFuM3NWiALtQTBrImJ+R0sOEixUP8WIywe\r\n"
    "qh+QeNJRANBQ/Q8CAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAIlukoutW/nWTgXL+\r\n"
    "IOQE4Prl50wFJZ+OY7f756YnRzto/C8Hwa1InrKE8Wku4W1LF34QveTaY8N4oiad\r\n"
    "LfGO3kmz49xDykt/HI/3GyhEI/5EHepfK+1cPKjnEDBy/2ya/YcFt6eTzHVXez8M\r\n"
    "cFaFqLax+5dbKk1V7gnXAIbdHSBkt1RE/fUszRY9cEGerHrhN8kuhft6ADyS8kIi\r\n"
    "DiyzCwXpkD9+4pcSokPgYxgCc9cGoHxJEL3NwAHcNZVFPn6QPQPhk5zQxVFwCQwn\r\n"
    "2GgGEfLgm7Bnc0+Cl39dd4M10DZdsOHjWUe6BTMoWWmSQczQB4+9jm4QrxqF9vZX\r\n"
    "pIg+uw==\r\n"
    "-----END CERTIFICATE-----\r\n";

static const char privateKey[] =
    "-----BEGIN PRIVATE KEY-----\r\n"
    "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDocyR07Wr3P7tP\r\n"
    "NNGEeoq+/zhiRMj12wH9zTNgpvMBEfb5beiCqydSCQTYS2P7yCwDnB3HZJznDabw\r\n"
    "VPxgitMrCD7KkObWFeuNpNMG/7mS9lAI9zpLeYv8wsjrtlUaMtJbroLCNC8ae1NF\r\n"
    "KkEKaKdLr2AaS4xFld6vJ7SobiQC99qjFaITbyBWe7DLr1GA77504riuq/jZIiL6\r\n"
    "rDB+pCasIIBr4FlONI2rGWhSxpI53FsgsCHz7dSZCUfim9OguiVuuY4fdFtHKjwO\r\n"
    "QmuoYA2DCLA8P7nySy/vMEz5RbjNzVogC7UEwayJifkdLDhIsVD/FiMsHqofkHjS\r\n"
    "UQDQUP0PAgMBAAECggEATXsb2tHMhs+6isNLV0JVfyapT7An4DN5LXU5u+zKMUAg\r\n"
    "t7GX//425BRXhhVTJXjqmuVt06nI1wdo7tEVMsTzcHOWIoU8PH4f/x3CraGN5Nuv\r\n"
    "f92Qk9nW/JCc0lz7bvkjvyWjNkKYtNYIwpQ8C9EoeoThnsIx5nv3NyEgCch0vnvm\r\n"
    "r+inwZ0YZB8oZpTMHyFJocdPnQM0wJQ5f9RPZtpAkAuTqy2IeN/SP7pElvrvBS8y\r\n"
    "RuudIXgiv0F2FUS6VsK5OnI/jzgZmDGaFllcXIpVJbkMctdiZy5KbDNkCBkScS+8\r\n"
    "vChAqGwFCp92Wpwny8L3Ak10krsKqI9CNBMJjEsw2QKBgQD8XW9wyXd/me19/AWP\r\n"
    "54CRyDnBD9H/gB+5FMt+EfU7IO9guYsQ8b7haJc591i+wX1NJT8SaLnywci5z1BF\r\n"
    "y9o5lfnhOOxH59LKb5RErO+NI41ObOSj/WfDiMWoGKbOo5uRkmm6oEx5gZ59joXu\r\n"
    "fPbXaHm64N1W56JwHGhWB7DmjQKBgQDrzEWw6qizafo6qnoETemm3fBUuT6eckyU\r\n"
    "8+JSvNJFb6aZA4r+nvMNYT3AI0EJSzo6dEPcNsDAvfLUwWHNGKf/qVEPc4Ol6ghl\r\n"
    "d8en8z1jHDOIA9XKs+YWmZtnXuoY1pnrMhWoZh0sS/c8rC/gHZCDHNiJ5tQWn+Ow\r\n"
    "YZCdkpSpCwKBgFgCIaX8rG7UeG0yIaJwwnaUlAePA9YNkYKTDPA3RQVM1A4wRp4v\r\n"
    "IDktp2dwqgNEKPVpYtiBqy9MgzrBYKYmlED224C+wC/HWcx9IwDcKmJXHqVV9lp2\r\n"
    "OeSH4x5fEnUHu24F2FdcJa9OE76HoI9uBwtSBPm2B5qSByT/0ycFYZzVAoGAYQd1\r\n"
    "xg9JzEpXxbxQ6WSGOTwqoTJ5KgouqTySbSl1/UysaVqj35LYwTC6kE3xfTmPhG5D\r\n"
    "XmLJfoarXhEVhagXuYSa7pjCG4vqWJapedsF77qzGc06NGdCzxn2cNsFyaLFQ5sg\r\n"
    "fmNGreu0mSrhqH+B7/H2yQhLTeQNdY8wSyb+NXECgYEAjFQn4koQi7F3cIKyfZws\r\n"
    "2AUY4FnKROe+JrDzuMBtSsLfIUYD3b6i2TncyYpIoMCxllChJr931/niA7bf5YBS\r\n"
    "9QtKKPfefBCGDN6vaHJ1gEAJYPVmRAFQBRbfW05hyPU0kXr1i4wFch7PGrwiZuxn\r\n"
    "LAyJAzoj2uHRucXIoMdcDEY=\r\n"
    "-----END PRIVATE KEY-----\r\n";
#endif
