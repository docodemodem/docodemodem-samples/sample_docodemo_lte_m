# どこでもでむのLTE-Mモデムを使ったサンプルプログラムです

センサー値をGoogle Spread Sheetに保存します。


GSS_IDはGoogle Spread Sheet毎に変更してください。下記URLの******部分がGSS_IDとなります。

https://docs.google.com/spreadsheets/d/******/edit?usp=sharing

Google Spread Sheet側のスクリプトにIDパラメータを渡すと指定のGSSに書き込みます。

セキュリティ的にはよろしくないので、使うときはご注意ください。


```
function doGet(e) {              
  if (e.parameter == undefined) {   
    result = 'Parameter undefined';
  }
  else {
    
    if( e.parameter.id == undefined){
      result = 'Parameter undefined id';
    }else{
      try{
        
        var sheet = SpreadsheetApp.openById(e.parameter.id).getActiveSheet();
        
        var newRow = sheet.insertRows(2,1);  // 次の行に入力する
        var rowData = [];       
        rowData[0] = new Date();   //タイムスタンプ
        
        for (var param in e.parameter) {
          console.log(param);
          if(isNaN(param))continue;
          var value = e.parameter[param];
          rowData[parseInt(param)] = value; 
        }
        
        var newRange = sheet.getRange(2, 1, 1, rowData.length);
        newRange.setValues([rowData]);
        
        var result =  'Ok';
      }catch(e){
        result = 'Parameter undefined spread sheet!!!!';
      }
    }
  }
  return ContentService.createTextOutput(result);
}
```