/*
    ご注意：
    ArduinoHttpClientライブラリを使うと色々便利ですが、なぜか通信が不安定です。細かくヘッダを送信しているためだと思います。
    送信データはまとめて送ったほうが安定して通信できます。
    GSSはリダイレクトするので、その処理をしないと戻り値が見れません。
    リダイレクトが来なくてもデータはGSSへ届いているケースが多いです。
*/

//Google spread sheet
//https://docs.google.com/spreadsheets/d/19mizSyhRrpybc6K-XhtoE_XoyP883EMy0D7Wj5mbNPs/edit?usp=sharing
//Google Action Script
//https://script.google.com/macros/s/AKfycbwmCahpagnqOWg78rkFSxGvoriXyzvayHOeD-F4rpMm6zaZLshWNWKs44WVI3YhQOGM/exec

#include "gss_task.h"
#include "HL7800ModemClient.h"

//#define USE_DISPLAY
#ifdef USE_DISPLAY
#include <SSD1306.h>
extern SSD1306 display;
const TickType_t xTicksToWait = 500UL;
#endif

extern SemaphoreHandle_t xSemaphoreDisplay;
void dispmsg(String msg);

#define USE_SSL
#ifdef USE_SSL
#include "ssl_keys.h"
#endif

extern HL7800Modem modemL;
extern HL7800ModemClient client;

const String _HOST = "script.google.com";
const int _PORT = 443;
const String DEPLOY_ID = "AKfycbwmCahpagnqOWg78rkFSxGvoriXyzvayHOeD-F4rpMm6zaZLshWNWKs44WVI3YhQOGM";
const String _MACRO_URL = "/macros/s/" + DEPLOY_ID + "/exec";

static xTaskHandle gss_task_handle;
static xTaskHandle adc_task_handle;

static float adjust_adVal(float val)
{
    //Adjustment
    //温度によってカーブが変わる
    float adj = round(val * 100); // 0.1234 -> 12 -> 0.01200
#if 1
    if (adj >= 1 && adj < 54)
    {
        adj += 3;
    }
    else if (adj >= 54 && adj < 62)
    {
        adj += 2;
    }
    else if (adj >= 62 && adj < 67)
    {
        adj += 1;
    }
    else if (adj >= 67 && adj < 73)
    {
    }
    else if (adj >= 73 && adj < 145)
    {
        adj -= 1;
    }
    else if (adj >= 145)
    {
        adj -= 2;
    }
#endif
    return adj / 100.0;
}

bool adc_task_enable = true;
static void adc_task(void *)
{
    portTickType xLastExecutionTime = xTaskGetTickCount();
    while (1)
    {
        vTaskDelayUntil(&xLastExecutionTime, 1000 / portTICK_PERIOD_MS);

#ifdef USE_DISPLAY
        if (adc_task_enable)
        {
            float adc = adjust_adVal(Dm.readExADC());
            if (xSemaphoreTake(xSemaphoreDisplay, xTicksToWait) == pdTRUE)
            {
                display.clear();
                display.drawString(0, 0, "waiting...");
                display.drawString(0, 20, "0-20 : " + String(adc * 10.0) + " mA");
                display.display();
                xSemaphoreGive(xSemaphoreDisplay);
            }
        }
#endif
    }

    vTaskDelete(NULL);
}

static bool set_ssl()
{
    if (!client.setCACert(rootCA))
        return false;
    if (!client.setCertificate(certificate))
        return false;
    if (!client.setPrivateKey(privateKey))
        return false;
    
    return true;
}

static bool reportGSS(){
    dispmsg("LTE-M sending...");
    if (client.connect(_HOST.c_str(), _PORT) > 0)
    {
        int rssi = modemL.getRssi();
        float temp = Dm.readTemperature();
        float press = Dm.readPressure() / 100;
        float hum = Dm.readHumidity();
        float adc = adjust_adVal(Dm.readExADC());
        //V -> mA
        adc = adc * 10;

        String params = _MACRO_URL;
        params += "1=" + String(temp, 1);
        params += "&2=" + String(press, 1);
        params += "&3=" + String(hum, 1);
        params += "&4=" + String(rssi);
        params += "&5=" + String(adc);
        SerialDebug.println("params: " + params);

        size_t ret = client.print(String("GET ") + params + " HTTP/1.1\r\n" +
                                  "Host: " + _HOST + "\r\n" +
                                  "Connection: close\r\n\r\n");
        if (ret == 0)
        {
            client.stop();
            SerialDebug.println("########## sendReport Send Error !! ###########\r\n");
            return false;
        }

        SerialDebug.println("done.");
        dispmsg("LTE-M send done");

        //リダイレクトを処理します。
        SerialDebug.println("#######  send ok,wait response");
        unsigned long timeout = millis();
        while (client.available() == 0)
        {
            SerialDebug.print("@");
            if (millis() - timeout > 10000)
            {
                SerialDebug.println(">>> Client Timeout !");
                client.stop();
                return true;
            }
        }

        String response = client.readString();

        //SerialDebug.println("### response Start");
        //SerialDebug.println(response);
        //SerialDebug.println("### response End...");

        if (response.indexOf("HTTP/1.1 302") >= 0)
        {
            client.stop();
            SerialDebug.println("######## start redirect");

            int locS = response.indexOf("https:");
            int locE = response.indexOf("\r\n", locS);
            int hostE = response.indexOf("/", locS + 8);

            //SerialDebug.println("\r\n########");
            //String AllRe = response.substring(locS, locE);
            //SerialDebug.println("All: " + AllRe);

            String reHost = response.substring(locS + 8, hostE);
            SerialDebug.println("redirect Host: " + reHost);

            String picUrl = response.substring(hostE, locE);
            SerialDebug.println("redirect url: " + picUrl);
            //SerialDebug.println("########\r\n");

            if (client.connect(reHost.c_str(), _PORT) > 0)
            {
                String getMsgRe = String("GET ") + picUrl + " HTTP/1.1\r\n" +
                                  "Host: " + reHost.c_str() + "\r\n" +
                                  "Accept: */*\r\n\r\n";

                size_t ret = client.print(getMsgRe);
                if (ret == 0)
                {
                    client.stop();
                    SerialDebug.println("########## Re;sendReport Send Error !! ###########\r\n");

                    return true;
                }

                //wait ack
                unsigned long timeout = millis();
                while (client.available() == 0)
                {
                    if (millis() - timeout > 10000)
                    {
                        SerialDebug.println(">>> Client Timeout !");
                        client.stop();
                        return true;
                    }
                    delay(100);
                }

                String response = client.readString();
                SerialDebug.println(response);

                SerialDebug.println("######## redirect done");
            }
            else
            {
                SerialDebug.println("######## redirect fail....");
            }
        }
        else if (response.indexOf("HTTP/1.1 500") >= 0)
        {
            SerialDebug.println("#### HTTP/1.1 500 Internal Server Error");
        }

        client.stop();
        Dm.LedCtrl(RED_LED, OFF);

#ifdef USE_DISPLAY
        if (xSemaphoreTake(xSemaphoreDisplay, xTicksToWait) == pdTRUE)
        {
            for (int i = 0; i < 3; i++)
            {
                display.clear();
                display.drawString(0, 0, "Temp: " + String(temp, 1) + " C");
                display.drawString(0, 20, "Press:" + String(press, 1) + " hPa");
                display.drawString(0, 40, "Hum:  " + String(hum, 1) + " %");
                display.display();
                delay(3000);

                display.clear();
                display.drawString(0, 0, "LTE-M: " + String(rssi) + " dBm");
                display.drawString(0, 20, "0-20 : " + String(adc) + " mA");
                display.display();
                delay(3000);
            }
            xSemaphoreGive(xSemaphoreDisplay);
        }
        display.clear();
#endif
    }
    else
    {
        // HTTP client errors
        SerialDebug.println("[HTTPS] no connection or no HTTP server.");
        dispmsg("LTE-M connect err");
        Dm.LedCtrl(RED_LED, ON);
    }

    return true;
}

static void gss_task(void *)
{
#ifdef USE_SSL
    if (!set_ssl()){
        SerialDebug.println("ssl set error. try again");
        if (!set_ssl())
        {
            SerialDebug.println("ssl set error again. reboot");
            delay(1000);
            ESP.restart();
        }
    }
#endif

    portTickType xLastExecutionTime = xTaskGetTickCount();
    while(1)
    {
        adc_task_enable = false;

        reportGSS();

        adc_task_enable = true;

        vTaskDelayUntil(&xLastExecutionTime, 1 * 60 * 1000 / portTICK_PERIOD_MS);
    }

    vTaskDelete(NULL);
}

void run_gss_task()
{
    dispmsg("Hello world!");

    xTaskCreatePinnedToCore(adc_task, "adc_task", 4096, NULL, 2, &adc_task_handle, 1);
    xTaskCreatePinnedToCore(gss_task, "gss_task", 4096, NULL, 2, &gss_task_handle, 1);
}