//https://docs.google.com/spreadsheets/d/19mizSyhRrpybc6K-XhtoE_XoyP883EMy0D7Wj5mbNPs/edit#gid=0
var GSS_ID = '19mizSyhRrpybc6K-XhtoE_XoyP883EMy0D7Wj5mbNPs';
var sheet_name = "log";

function doPost(e) {
    var result = 'Post!';

    try {
        if (e.parameter == undefined) {
            result = 'Parameter undefined';
        }
        else {

            var sheet = SpreadsheetApp.openById(GSS_ID).getSheetByName(sheet_name);

            sheet.insertRows(2, 1);  // 次の行に入力する
            var rowData = [];
            rowData[0] = new Date();   //タイムスタンプ

            for (var param in e.parameter) {
                //console.log(param);
                if (Number.isNaN(param)) continue;
                var value = e.parameter[param];
                rowData[parseInt(param)] = value;
            }

            //値書き込み
            var newRange = sheet.getRange(2, 1, 1, rowData.length);
            newRange.setValues([rowData]);

            result = "Post! " + rowData[0].toString();
        }
    } catch (ex) {
        result = "exception";
    }

    return ContentService.createTextOutput(result);
}

function doGet(e) {
    var result = 'Get!';

    try {
        if (e.parameter == undefined) {
            result = 'Parameter undefined';
        }
        else {

            var sheet = SpreadsheetApp.openById(GSS_ID).getSheetByName(sheet_name);

            sheet.insertRows(2, 1);  // 次の行に入力する
            var rowData = [];
            rowData[0] = new Date();   //タイムスタンプ

            for (var param in e.parameter) {
                //console.log(param);
                if (Number.isNaN(param)) continue;
                var value = e.parameter[param];
                rowData[parseInt(param)] = value;
            }

            //値書き込み
            var newRange = sheet.getRange(2, 1, 1, rowData.length);
            newRange.setValues([rowData]);

            result = "Get! " + rowData[0].toString();
        }
    } catch (ex) {
        result = "exception";
    }

    return ContentService.createTextOutput(result);
}