
#include "mqtt_task.h"
#include "modem_setting.h"
#include <PubSubClient.h>
#include "HL7800ModemClient.h"

#define USE_SSL

#ifdef USE_SSL
#include "ssl_keys.h"
#endif

extern HL7800ModemClient client;

static xTaskHandle mqtt_task_handle;

static const char *host = "hogehoge.jp";
static uint16_t port = 8883;//1883;

static PubSubClient mqttClient;
static const char *topic = "/hello"; // 送信先のトピック名

void callback(char *topic, byte *payload, unsigned int length)
{
    SerialDebug.print("Subscribe:");
    for (int i = 0; i < (int)length; i++)
        SerialDebug.print((char)payload[i]);
    SerialDebug.println("");
}

static bool connectMqtt()
{
    SerialDebug.println("### Connecting to MQTT server");

    String clientId = "DOKODEMO-0001";
    if (mqttClient.connect(clientId.c_str()))
    {
        SerialDebug.println("### connected");
        return true;
    }
    else
    {
        SerialDebug.println("### Failed connection...");
        return false;
    }
}

static int count = 0;
unsigned long lastMillis = 0;

static void mqtt_task(void *)
{
#ifdef USE_SSL
    client.setCACert(rootCA);
    client.setCertificate(certificate);
    client.setPrivateKey(privateKey);
#endif

    mqttClient.setServer(host, port);
    mqttClient.setCallback(callback);
    mqttClient.setClient(client);

    connectMqtt();

    while (1)
    {
        mqttClient.loop();

        if (millis() - lastMillis > 1 * 60 * 1000)
        {
            lastMillis = millis();
            if (!client.connected())
            {
                connectMqtt();
            }

            char buf[20];
            sprintf(buf, "world%04d", count++);

            SerialDebug.println("### publish...");
            if (mqttClient.publish(topic, buf))
            {
                SerialDebug.println("### OK");
            }
            else
            {
                SerialDebug.println("### NG...");
            }
        }
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }

    vTaskDelete(NULL);
}

void run_mqtt_task()
{
    xTaskCreatePinnedToCore(mqtt_task, "mqtt_task", 8192, NULL, 2, &mqtt_task_handle, 1);
}
