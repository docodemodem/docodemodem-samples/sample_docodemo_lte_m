#include "simple_modem_task.h"
#include "HL7800ModemClient.h"

#define USE_SSL

#ifdef USE_SSL
#include "ssl_keys.h"
#endif

extern HL7800Modem modemL;
extern HL7800ModemClient client;

static xTaskHandle simple_modem_task_handle;
static const char *host = "hogehoge.jp";
static int port = 3000;

static void http_get()
{
  SerialDebug.println("\n###start connection");
  if (!client.connect(host, port))
  {
    SerialDebug.println("###error");
    return;
  }

  String url = "/";

  SerialDebug.printf("###Requesting URL: %s\n", url.c_str());

  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");

  delay(10);

  unsigned long timeout = millis();
  while (client.available() == 0)
  {
    if (millis() - timeout > 5000)
    {
      timeout = millis();
      SerialDebug.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }

  // Read all the lines of the reply from server and print them to Serial
  while (client.available())
  {
    String line = client.readStringUntil('\n');
    SerialDebug.println(line);
  }

  SerialDebug.printf("###RSSI: %d\n", modemL.getRssi());
  char ip[16];
  if (modemL.getIPaddress(ip))
  {
    SerialDebug.printf("####ip: %s\n", ip);
  }

  SerialDebug.println("\n\n");
}

static void simple_modem_task(void *)
{
#ifdef USE_SSL
  //client.setCACert(GoogleSpreadSheet_rootCA);
  client.setCACert(rootCA);
  client.setCertificate(certificate);
  client.setPrivateKey(privateKey);
#endif

  portTickType wakeupTime = xTaskGetTickCount();
  while (1)
  {
    http_get();
    vTaskDelayUntil(&wakeupTime, 30 * 1000 / portTICK_PERIOD_MS);
  }
}

void run_simple_modem_task()
{
  xTaskCreatePinnedToCore(simple_modem_task, "simple_modem_task", 4096, NULL, 2, &simple_modem_task_handle, 1);
}
