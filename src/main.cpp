#include <docodemo.h>
#include "HL7800ModemClient.h"
#include "modem_setting.h"
#include <driver/uart.h>

//#define USE_DISPLAY
#ifdef USE_DISPLAY
#include <SSD1306.h>
TwoWire I2C_external = TwoWire(1);
SSD1306 display(0x3c, EXTERNAL_SDA, EXTERNAL_SCL, GEOMETRY_128_64, I2C_TWO, 400000);
const TickType_t xTicksToWait = 500UL;
#endif
SemaphoreHandle_t xSemaphoreDisplay = NULL;

//#define SIMPLE
//#define MQTT
#define GSS_TCP

#ifdef SIMPLE
#include "simple_modem_task.h"
#endif

#ifdef MQTT
#include "mqtt_task.h"
#endif

#ifdef GSS_TCP
#include "gss_task.h"
#endif

DOCODEMO Dm = DOCODEMO();

HL7800Modem modemL;
HL7800ModemClient client(&modemL);

void dispmsg(String msg)
{
#ifdef USE_DISPLAY
  if (xSemaphoreTake(xSemaphoreDisplay, xTicksToWait) == pdTRUE)
  {
    display.clear();
    display.drawString(0, 0, msg);
    display.display();
    xSemaphoreGive(xSemaphoreDisplay);
  }
#endif
}

//Arduinoの関数ではハードウェアフロー制御を使えないのでesp-idfを使います。
int writeUart(const uint8_t *data, uint32_t len)
{
  return uart_write_bytes(UART_NUM_2, (const char *)data, len);
}

int readUart(uint8_t *data, uint32_t len, uint32_t timeout)
{
  int length = 0;

  uart_get_buffered_data_len(UART_NUM_2, (size_t *)&length);
  if (length == 0){
    vTaskDelay(1);//タスク切り替えを許可する場合は実行する。
    return 0;
  }

  return uart_read_bytes(UART_NUM_2, data, len, timeout / portTICK_RATE_MS);
}

bool modem_init()
{
  dispmsg("initialize LTE-M");

  Dm.ModemPowerCtrl(OFF);
  vTaskDelay(10);
  Dm.ModemPowerCtrl(ON);

  //for LTE-M modem initialize
  pinMode(RF_WAKEUP_OUT, OUTPUT);
  digitalWrite(RF_WAKEUP_OUT, HIGH);

  //Arduinoの関数ではハードウェアフロー制御を使えないのでESP32ではesp-idfを使います。
  uart_config_t uart_config = {
      .baud_rate = 115200,
      .data_bits = UART_DATA_8_BITS,
      .parity = UART_PARITY_DISABLE,
      .stop_bits = UART_STOP_BITS_1,
      .flow_ctrl = UART_HW_FLOWCTRL_CTS,
      .rx_flow_ctrl_thresh = 122,
  };

  uart_param_config(UART_NUM_2, &uart_config);
  uart_set_pin(UART_NUM_2, MODEM_UART_TX_PORT, MODEM_UART_RX_PORT, UART_PIN_NO_CHANGE, RF_CTS_IN);
  uart_driver_install(UART_NUM_2, 256, 0, 0, NULL, 0);
  uart_flush(UART_NUM_2);

  if (!modemL.init(&writeUart, &readUart))
  {
    SerialDebug.println("###Error");
    dispmsg("LTE-M Hard err");
    return false;
  }

  SerialDebug.println("###OK\n");

  SerialDebug.println("###Start activate...");
  dispmsg("LTE-M activation");
  if (!modemL.activate(APN, USERNAME, PASSWORD))
  {
    SerialDebug.println("###Error");
    return false;
  }
  SerialDebug.println("###OK\n");
  dispmsg("LTE-M OK");

  return true;
}

void setup()
{

  xSemaphoreDisplay = xSemaphoreCreateBinary();
  configASSERT(xSemaphoreDisplay);
  xSemaphoreGive(xSemaphoreDisplay);

#ifdef USE_DISPLAY
  display.init(); //ディスプレイを初期化
  display.flipScreenVertically();

  display.setFont(ArialMT_Plain_16);       //フォントを設定
  display.drawString(0, 0, "Hello Wolrd"); //(0,0)の位置にHello Worldを表示
  display.display();                       //指定された情報を描画
#endif

  SerialDebug.begin(115200);
  SerialDebug.println("### Start!");

  Dm.begin();
  Dm.ModemPowerCtrl(OFF);

  Dm.LedCtrl(RED_LED, OFF);
  Dm.LedCtrl(GREEN_LED, ON);

#if 1
  if(!modem_init()){
    SerialDebug.println("modem_init error, try again...");
    Dm.ModemPowerCtrl(OFF);
    delay(100);
    if (!modem_init())
    {
      SerialDebug.println("modem_init error,again...");
      vTaskDelete(NULL);
      Dm.LedCtrl(RED_LED, ON);
      Dm.LedCtrl(GREEN_LED, OFF);
      return;
    }
  }
#endif

#ifdef SIMPLE
  run_simple_modem_task();
#endif

#ifdef MQTT
  run_mqtt_task();
#endif

#ifdef GSS_TCP
  run_gss_task();
#endif

  vTaskDelete(NULL);
}

void loop()
{
  //not get here
}
